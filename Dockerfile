FROM ubuntu

WORKDIR /app

COPY . /app

ENV NODE_VERSION 10.12.0

RUN apt-get update && apt-get install -y git curl && curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash && export NVM_DIR="$HOME/.nvm" && [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" && [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" && nvm install $NODE_VERSION && nvm use $NODE_VERSION && npm install

EXPOSE 3000

ENV NODE_PATH ~/.nvm/versions/node/v$NODE_VERSION/bin
ENV PATH $NODE_PATH:$PATH


CMD ["/bin/bash","-c","node index.js"]